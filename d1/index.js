// console.log("Hello World!");
//mini activity print your name in the console
// F12 or Ctrl + Shift + C to access the console
// console.log("Mayen D. Comia");
// Ctrl / for comments

// Section: Syntax, Statements, and Comments

// Statement in programming are instructions that we tell the computer to perform
	//usually ends with semicolon ;
	//trains us to locate where a statement ends
	//alert("Hello");
	//console.log ("Hi");

//Syntax in programming, it is the set of rules that describes how statements must be constructed
//All lines/blocks of code should be written in a specific manner to work
//This is due to how these codes were initially programmed to function and perform in a certain manner

//Where to Place JavaScript
	//Inline - you can place JS right into the HTML page using the script tags
		//for very small sites and testing only
		//the inline approach does not scale well, leads to poor organization and code duplication
	//External - better approach
		//place JS into separate files and link to them from the HTML page
		//this approach is much easier to maintain, write and debug

//Use of Script Tag
	//script tag can go anywhere on the page
	//as a best practice, many developers will place it just before the closing body tag on the HTML page.
	//This provides faster speed load times for our web page

//We use devtools also to DEBUG, view messages and run JavaScript code in the console tab

		console.log("Hello");
//Whitespace can impact functionality in many computer languages-BUT not in JavaScript. In JS, whitespace is used only for readability and has no functional impact. One effect of this is a single statement that can span multiple times.

			console.log("Hello World1");

			console. log("Hello World2");

			console. 
			log
			(
				"Hello World3"
				);

//Comments
	//Comments are parts of the code that gets ignored by the language
	//Comments are meant to describe the written code

			/*
				There are two types of comments:
				1. Single-line comment denoted by two slashes (ctrl /)
				2. Multi-line comment denoted by a slash and asterisk (ctrl shift /)
			*/

//Variables
	//it is used to contain data
	//any information that is used by our applications are stored in what we call a memory
	//when we create variables, certain portions of a device's memory is given a "name" that we call "variables"

	//this makes it easier for us to associate information stored in our devices to actual "names" about information

			let x = 1;
			let y

	//Declaring variables
		//tells our devices that a variable name is created and is ready to store data
		//declaring a variable without giving it a value will automatically assign it with the value of "undefined" meaning that the variable's value is "not defined"

	//Syntax
		//let/const variableName;
		//let/const variableNameOne;

	//let is a keyword that is usually used in declaring a variable

		let myVariable;

		//console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console

		console.log(myVariable); //undefined

		// console.log(hello); //undefined

		//variables must be declared first before they are used
		//using variables before they are declared will return an error
		let hello; 

		/*
			Guidelines in writing variables:
			1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value
			2. Variable names should start with a lowercase character,
			use camelCase for multiple words
			3. For Constant variables, use the 'const' keyword
			4. Variable names should be indicative or descriptive of the value being stored to avoid confusion

			Best practices in naming variables

			1. 

			let firstName = "Michael"; //good variable name
			let pokemon = "Charizard"; //good
			let pokemon = 25000; //bad

			2. 

			let FirstName = "Michael"; //bad
			let lastName = "Jordan"; //good

			3. 

			let first name = "Mickey"; //bad
			let firstName = "Mickey"; //good

			lastName emailAddress mobileNumber internetAllowance

			underscore
			let product_description = "cool product";


		*/ 

//Declaring and initializing variables
	//Initializing variables - the instance when a variable is given its initial/starting value

	//Syntax
		//let/const variableName = value;

		let productName = "desktop computer";
		console.log(productName);

		let productPrice = 30000;
		console.log(productPrice);

		//in the contex of certain applications, some variables/information are constant and should not be changed
		//in this example, the interest rate for a loan, sabings account or mortgage muist not be changed due to real world concerns

		const interest = 3.539;

		//let- we usually use let if we want to reassign the value in our variable

		//reassign variable values
		//means changing its initial or previous value into another value

		// Syntax
			// variableName = newValue;

		productName = 'laptop';

		console.log(productName);

		// let friend = "Kate";
		// friend = 'Jane';

		let friend = "Kate";
		// let friend = "Jane";//error friend has already been declared

		//interest = 4.4888;//error const

		let supplier;//a. declaration

		supplier = "John Smith Tradings";//b. initialization
		console.log(supplier);

		supplier = "Zuitt Store";//c. reassign
		console.log(supplier);

		//example: will return an error const should have initial value already
		// const pi;
		// pi=3.1416;
		// console.log(pi);

		//let/const local/global scope

		//scope means where these variables are available for use
		//let and const are block scoped
		//block is a chunk of code bounded by {}
		//a block lives in curly braces
		//anything within the curly braces is a block
		//so a variable declared in a block with let is only available for use within that block

		let outerVariable = 'hello';
		
		{
			let innerVariable = "hello again";
			console.log(innerVariable);
		}

		console.log(outerVariable);//hello
		//console.log(innerVariable);//undefined

//Multiple variable declarations
//multiple variables may be declared in one line
//though it is quicker to do without having to retype the let keyword


let productCode = "CD248"; //best practice
let	productBrand = "Dell";

console.log(productCode, productBrand); //CD248 Dell

//const let = "hello";
//console.log(let); //syntax error

//Section: Data Types

	//Strings
	//series of characters that create a word, a phrase, a sentence or anything related to creating text
	//in JS can be written using a single or double quote
	//in other programming language only the double quotes can be used for creating strings

	let country = "Philippines";
	let province = "Metro Manila";

	//Concatenating strings
	//multiple string values can be combined to create a single string using the + symbol

	let fullAddress = province + "," + country
	console.log(fullAddress)

	let greeting = "I live in the " + country;
	console.log(greeting);//


	//the escape character (\) in strings in combination with other characters can produce different effects

	//"\n" refers to creating a new line between texts
	let mailAddress = "Metro Manila\n\n\nPhilippines";
	console.log(mailAddress);

	let message = "John's employees went home early";
	console.log(message);//John's employees went home early
	message = 'John\'s employees went home early'
	console.log(message);

	//Numbers
	//Integers/Whole Numbers
	let headcount = 23;
	console.log(headcount);//23

	//Decimal Numbers/Fractions
	let grade = 98.7;
	console.log(grade);//98.7

	//Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);//20000000000

	//Combine strings and numbers

	console.log("John's grade last month is "+ grade);//John's grade last month is 98.7

	//Boolean
	//boolean values are normally used to store values relating to the state of certain things
	//this will be useful in further discussions about creating logic to make our application respond to certain scenarios

	let isMarried = false;
	let inGoodConduct = true;

	console.log("isMarried:"+ isMarried);
	console.log("inGoodConduct:" + inGoodConduct);

	//Arrays
	//Arrays are a special kind of data type that is used to store multiple values
	//Arraus can store different data types but it is normally to similar data types

	//similar data types
	//syntax
		//let/const arrayName = [elementA, elementB, elementC, ...]

	let grades = [98.7, 92.1, 90.2, 94.6];//similar data types
	console.log(grades); //

	let details = [ "john", "smith", 32, true];
	console.log(details);

	//Objects
		//Objects are another special kind of data type that's used to mimic real world objects/items
		//They are used to create complex data that contains pieces of information that are relevant to each other 
		//every individual piece of info is called a property of the object

		//syntax
		//let/const objectName = {
		//propertyA: value;
		//propertyB: value;
		//}

	let person = {
		firstName: "Albedo",
		lastName: "Cardo",
		age: 35,
		isMarried: false,
		contact: ["09123456789", "0987654321"],
		address: {
			houseNumber: "248",
			city: "Quezon City"
		}
	}
	console.log(person);

	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6
	}

	console.log(myGrades);
	console.log(typeof myGrades);//object

	//const anime = ['OP', 'OPM' , 'AOT'];
	//anime = ['KNY'];
	//console.log(anime);

	//OP[0], OPM[1]
	const anime = ['OP', 'OPM' , 'AOT'];
	anime[0] ='KNY';
	console.log(anime);//['KNY', 'OPM' , 'AOT']


	//Null
	//it is used to intentionally express the absence of a value in a variable declaration/initialization
	//null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

	let spouse = null;

	//using null compared to a 0 value and an empty string is much better for readability purposes
	//null is also considered as a data type of its own compared to 0 which is a data type of a number and a single quotes which are a data type of a string

	let myNumber = 0;
	let myString = '';

	//Undefined
	//represents the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName);//undefined
	